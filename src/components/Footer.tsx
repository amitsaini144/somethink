
function Footer() {
    return (
        <footer className="text-center p-4 md:p-6 bg-gray-700 text-white z-50">
            © 2024 SomeThink. All rights reserved.
        </footer>
    )
}

export default Footer